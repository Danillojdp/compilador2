lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';
LCOLCHETE: '[';
RCOLCHETE: ']';
PVIRGULA:';';
LPARENT:'(';
RPARENT:')';
VIRG:',';
MENOS:'-';
EXCLAMA:'!';
CARACT: 'char*';
MAIS:'+';
LBAR:'/';
ASTER:'*';
MENOR:'<';
MAIOR:'>';
DIFERENTE:'!=';
MENORIGUAL:'<=';
MAIORIGUAL:'>=';
MAISIGUAL:'+=';
MENOSIGUAL:'-=';
ECOMERCIAL:'&';
AND:'&&';
IGUAL:'=';
IGUALDADE:'==';
BARRA:'|';
OR:'||';
PERCENT:'%';


BOOLEAN: 'boolean';

BREAK: 'break';

CALLOUT: 'callout';

CLASS: 'class';

CONTINUE: 'continue';

ELSE: 'else';

FALSE: 'false';

FOR: 'for';

INT: 'int';

RETURN: 'return';

TRUE: 'true';

VOID: 'void';

IF: 'if';

fragment ID  :  ('a'..'z' |'A'..'Z');

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHARLITERAL : '\'' (ESC|CHAR) '\'';

STRING : '"' (ESC|CHAR)* '"';

IDEN : ('_'|ID) (ID|'_'|NUM)*;

//OPERAHEXA : '0x'(NUM|ID|'_')+ | NUM+ ~'x';

NUMERO: OPERAHEXA|INTEIRO;

fragment OPERAHEXA: '0x'(LETTER)+;

fragment INTEIRO: NUM+;

//OPERADORES: ('+' | '/' | '*' | '<' | '>' | '!='| '<=' | '>=' | '&&' | '=' | '|' | '||');

fragment NUM: [0-9]+;

fragment ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');

fragment LETTER: 'a'..'z'|'A'..'Z'|'_';

fragment CHAR: (' '|'!'|'#'|'$'|'%'|'&'|'('|')'|'*'|'+'|','|'-'|'.'|'/'|'0'..'9'|':'|';'|'<'|'='|'>'|'?'|'@'|'A'..'Z'|'['|']'|'^'|'_'|'`'|'a'..'z'|'{'|'|'|'}'|'~');


