package decaf;
import org.antlr.symtab.FunctionSymbol;
import org.antlr.symtab.GlobalScope;
import org.antlr.symtab.LocalScope;
import org.antlr.symtab.Scope;
import org.antlr.symtab.VariableSymbol;
import org.antlr.symtab.Symbol;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;


/**
 * This class defines basic symbols and scopes for Decaf language
 */
public class DecafSymbolsAndScopes extends DecafParserBaseListener {
    ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
    GlobalScope globals;
    Scope currentScope; // define symbols in this scope

    @Override
    public void enterProgram(DecafParser.ProgramContext ctx) {
        globals = new GlobalScope(null);
        pushScope(globals);
	
    }

    @Override
    public void exitProgram(DecafParser.ProgramContext ctx) {      
System.out.println(globals);
    }

    @Override
    public void enterMethod_decl(DecafParser.Method_declContext ctx) {      
String name = ctx.IDEN().getText();
        int typeTokenType = ctx.type().start.getType();
        DecafSymbol.Type type = this.getType(typeTokenType);

        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        // function.setType(type); // Set symbol type

        currentScope.define(function); // Define function in current scope
        saveScope(ctx, function);
        pushScope(function);
    }

    @Override
    public void exitMethod_decl(DecafParser.Method_declContext ctx) {
        popScope();
    }

    @Override
    public void enterBlock(DecafParser.BlockContext ctx) {
        LocalScope l = new LocalScope(currentScope);
        saveScope(ctx, currentScope);
        // pushScope(l);
    }

    @Override
    public void exitBlock(DecafParser.BlockContext ctx) {
        //popScope();
    }

    @Override
    public void enterField(DecafParser.FieldContext ctx) {
          if (ctx.NUMERO() != null) {
            if (Integer.parseInt(ctx.NUMERO().getText()) <= 0)
                error(ctx.NUMERO().getSymbol(), "Vetor não pode possuir o valor 0");
            //System.out.println("aaa");
        }
    }
	
boolean verificarVar(String nomeID){
        boolean valor = false;
        String [] a, b;
        if (!currentScope.getName().equals("global")) {
            a = String.valueOf(currentScope).split(currentScope.getName());
            b = a[1].split("\\[");
        }else {
            b = String.valueOf(currentScope).split("\\[");

        }

        //System.out.println(b[1]);
        if (!b[1].equals("]")) {
            String[] c = b[1].split("\\]");
            //System.out.println(c[0]);
            String[] e = c[0].split("\\,");
            //System.out.println(e.length);
            for (int i = 0; i < e.length; i++) {
                //System.out.println(e[i]);
                if (e[i].trim().equals(nomeID)) {
                    //System.out.println("existe");
                    valor = true;
                    break;
                }
            }

        }
        //System.out.println(valor);
        return valor;
        /*System.out.println(e.length);
        System.out.println(e[0]);
        System.out.println(e[1]);*/
    }


    @Override
    public void exitField(DecafParser.FieldContext ctx) {
	if (ctx.IDEN()!=null){       
	String name = ctx.IDEN().getSymbol().getText();
	
        Symbol var = currentScope.resolve(name);
        if ( var==null ) {
            this.error(ctx.IDEN().getSymbol(), "no such variable: "+name);
        }
        if ( var instanceof FunctionSymbol ) {
            this.error(ctx.IDEN().getSymbol(), name+" is not a variable");
        }
	}
    	}

	@Override public void enterMethod(DecafParser.MethodContext ctx) {
	if(ctx.type() != null){
			defineVar(ctx.type(), ctx.IDEN().getSymbol());
		}else{
			defineVar(ctx.IDEN().getSymbol());
	 	}
 	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMethod(DecafParser.MethodContext ctx) { 
	String name = ctx.IDEN().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        if ( var==null ) {
            this.error(ctx.IDEN().getSymbol(), "no such variable: "+name);
        }
        if ( var instanceof FunctionSymbol ) {
            this.error(ctx.IDEN().getSymbol(), name+" is not a variable");
        }
}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */	


    void defineVar(DecafParser.TypeContext typeCtx, Token nameToken) {
        int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }
	
	void defineVar(Token nameToken) {
        //int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }


    /**
     * Método que atuliza o escopo para o atual e imprime o valor
     *
     * @param s
     */
    private void pushScope(Scope s) {
        currentScope = s;
        System.out.println("entering: "+currentScope.getName()+":"+s);
    }
	
	
    /**
     *
     * @param ctx
     * @param s
     */
    void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
     * Muda para o contexto superior e atualia o escopo
     */
    private void popScope() {
        System.out.println("leaving: "+currentScope.getName()+":"+currentScope);
        currentScope = currentScope.getEnclosingScope();
    }

    public static void error(Token t, String msg) {
        System.err.printf("line %d:%d %s\n", t.getLine(), t.getCharPositionInLine(),
                msg);
    }

    /**
     * Valida tipos encontrados na linguagem para tipos reais
     *
     * @param tokenType
     * @return
     */
    public static DecafSymbol.Type getType(int tokenType) {
        switch ( tokenType ) {
            case DecafParser.VOID :  return DecafSymbol.Type.tVOID;
            case DecafParser.NUMERO :   return DecafSymbol.Type.tINT;
        }
        return DecafSymbol.Type.tINVALID;
    }


}
