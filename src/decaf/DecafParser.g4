parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}


program: TK_class LCURLY field_decl* method_decl*  RCURLY EOF;

field: type IDEN | method LCOLCHETE NUMERO RCOLCHETE ;

field_decl: field (VIRG field)* PVIRGULA;

method: type? IDEN;

method_decl:  type IDEN LPARENT (method (VIRG method)*)* RPARENT block;

block: LCURLY var_decl* statement* RCURLY;

var_decl: method (VIRG method)* PVIRGULA;

type: INT | BOOLEAN | VOID;

statement: location assign_op expr PVIRGULA 
	| method_call PVIRGULA 
	| IF LPARENT expr RPARENT block ( ELSE block )* 
	| FOR IDEN IGUAL expr VIRG expr block 
	| RETURN expr* PVIRGULA 
	| BREAK PVIRGULA 
	| CONTINUE PVIRGULA 
	| block;

assign_op: IGUAL | MAISIGUAL | MENORIGUAL;


method_call: method_name LPARENT (expr (VIRG expr)*)* RPARENT | CALLOUT LPARENT STRING (VIRG callout_arg)* RPARENT;

method_name: IDEN;

location: IDEN | IDEN LCOLCHETE expr RCOLCHETE;

expr: location | method_call | literal | expr bin_op expr | MENOS expr | EXCLAMA expr | LPARENT expr* RPARENT;

callout_arg: STRING|expr  ;

bin_op: arith_op | rel_op | eq_op | cond_op;

arith_op:( MAIS | MENOS | ASTER | LBAR | PERCENT) ;

rel_op: ( MENOR | MAIOR | MENORIGUAL | MAIORIGUAL) ;

eq_op: IGUALDADE | DIFERENTE;

cond_op: AND | OR;

literal: (NUMERO | CHARLITERAL | BOOLEANLITERAL);






